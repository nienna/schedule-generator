# <img src="gui/media/magic-lamp.png" height="64px" style="vertical-align: middle;"> <sub style="font-size: 1.3em">Schedule Genie</sub>

------

College work, Schedule Generator. From the subjects, classrooms and restrictions introduced Schedule Genie creates a timetable for the university.

## How to use Schedule Genie

To use Schedule Genie just run the file [ScheduleGenie.jar] (ScheduleGenie.jar) located in the root folder.

> You must have java 8 installed

For console you will be asked to enter a <small> Curriculum </small> and a Campus. You must enter the name <small> (without extension) </small> of a file saved in the [data / curriculum /] folder (`data / curriculum /`) and one of [data / campus /] (data / campus /). If you want to create subjects or classrooms, you only need to create or modify the .json in the [data /] (data /) folders for testing.

The next parameter to enter is the name under which the generated schedule will be saved.

Then the program will generate a schedule with all the default sessions and restrictions. This schedule will be saved in [data / schedule /] (data / schedule /) with the name previously entered in JSON format.

To view the schedule we have made a very basic web page, located at [gui / index.html] (gui / index.html). To see the schedule, simply enter the generated JSON on the web.

### How to compile and run

```bash
rm -rf exe/*
javac src/presentation/MainMenu.java -d exe/ -cp src/
cd exe/ && jar cfe ../ScheduleGenie.jar presentation/MainMenu . && cd ..
java -jar ScheduleGenie.jar
```

### Folder structure

- **data**: it contains the files that store the information, it is the "Database"
- **guide**: contains a simple schedule viewer.
- **src**: contains the source code.
   - **data**: contains the data layer files.
   - **domain**: contains the files of the domain layer.
   - **presentation**: contains the files of the presentation layer.
   - **org**: contains the org.json library.

